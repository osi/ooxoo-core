package com.idyria.osi.ooxoo.core.buffers.datatypes

import com.idyria.osi.ooxoo.core.buffers.structural.AbstractDataBuffer

import scala.language.implicitConversions

import java.util._
import java.text._

/**
 * DateTimeBuffer bears per default the time at object creation
 *
 */
class DateTimeBuffer extends AbstractDataBuffer[java.util.GregorianCalendar] with Comparable[java.util.GregorianCalendar] {

  // Default Constructor
  //--------------------
  this.data = new GregorianCalendar

  def dataFromString(str: String): java.util.GregorianCalendar = {

    //this.data = java.lang.Boolean.parseBoolean(str)
    //this.data

    // Parse
    //-------------
    var dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
    var date = dateFormat.parse(str, new ParsePosition(0))
    if (date == null) {
      throw new RuntimeException(s"""Could not parse date: $str , does not match correct format: yyyy-MM-dd'T'HH:mm:ssX""")
    }

    // Create Gregoran Calendar from this date
    //----------------
    this.data = new GregorianCalendar

    /* println(s"parsed with offset: ${date.getTimezoneOffset()}")
        // The Timezone offset is relative to the current one in seconds
        this.data.getTimeZone.setRawOffset((date.getTimezoneOffset()))*/

    this.data.setTime(date)
    this.data

  }

  def dataToString: String = if (data != null) String.format("%1$tY-%1$tm-%1$tdT%1$tH:%1$tM:%1$tS%tz", this.data);

/*
 * #%L
 * Core runtime for OOXOO
 * %%
 * Copyright (C) 2008 - 2014 OSI / Computer Architecture Group @ Uni. Heidelberg
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
else null

  override def toString: String = this.dataToString

  def equals(comp: DateTimeBuffer): java.lang.Boolean = this.data == comp.data

  def compareTo(comp: java.util.GregorianCalendar): Int = this.data.compareTo(comp)

}

object DateTimeBuffer {

  def apply() = new DateTimeBuffer
  
  implicit def convertDateTimeBufferToCalendar(b: DateTimeBuffer): java.util.GregorianCalendar = b.data
  implicit def convertCalendarToDateTimeBuffer(c: java.util.GregorianCalendar): DateTimeBuffer = {

    var dtb = new DateTimeBuffer
    dtb.data = c
    dtb

  }
}
