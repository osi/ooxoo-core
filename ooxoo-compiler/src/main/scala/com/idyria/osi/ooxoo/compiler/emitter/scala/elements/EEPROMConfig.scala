
/**
 *
 */
package com.idyria.osi.ooxoo.compiler.emitter.scala.elements;



/**	
 *The target indicated the eeprom target, in case
						there would be more than one on a board
 */
/**	
 *
						Indicates the data with at each address (Typical 8
						bits so 1 byte)
					
 */
/**	
 *
						The global size of the eeprom
					
 */
class EEPROMConfig extends com.idyria.osi.ooxoo3.core.buffers.structural.ElementBuffer {
    
    
     
	var  EEPROMData : com.idyria.osi.ooxoo3.core.buffers.structural.XList[com.idyria.osi.ooxoo.core.buffers.common.ObjectBuffer] = null


}       
