/**
 * 
 */
package com.idyria.osi.ooxoo.compiler.xsd;

/**
 * @author Rtek
 *
 */
public class ElementHasNoValueException extends Exception {

	/**
	 * 
	 */
	public ElementHasNoValueException() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ElementHasNoValueException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public ElementHasNoValueException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param arg0
	 */
	public ElementHasNoValueException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	
	
}
