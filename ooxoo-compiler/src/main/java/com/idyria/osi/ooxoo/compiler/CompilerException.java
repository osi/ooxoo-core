/**
 * 
 */
package com.idyria.osi.ooxoo.compiler;

/**
 * @author rtek
 *
 */
public class CompilerException extends Exception {

	/**
	 * 
	 */
	public CompilerException() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public CompilerException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public CompilerException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CompilerException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
